﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OnlineForm.aspx.cs" Inherits="Assignment_1a.OnlineForm" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="onlineForm1" runat="server">
        <div>
            <h1>Handmade Craft</h1>
            <%-- Customer Information --%>
            <fieldset><legend>Customer Info</legend>
            <label>First Name:</label>
            <asp:TextBox runat="server" ID="customerFName" placeholder="First Name"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ID="validatorFName" ControlToValidate="customerFName" 
                ErrorMessage="Please enter a first name."></asp:RequiredFieldValidator>
            <br /><br />
            <label>Last Name:</label>
            <asp:TextBox runat="server" ID="customerLName" placeholder="Last Name"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ID="validatorLName" ControlToValidate="customerLName" 
                ErrorMessage="Please enter a last name."></asp:RequiredFieldValidator>
            <br /><br />
            <label>Gender</label>
            <asp:RadioButton runat="server" Text ="Male" GroupName ="gender" marked="true"/>
            <asp:RadioButton runat="server" Text ="Female" GroupName ="gender" />
            <br /><br />
            <label>Phone:</label>
            <asp:TextBox runat="server" ID="customerPhone"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ID="validatorPhone" ControlToValidate="customerPhone"
                ErrorMessage="Please enter phone number."></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator runat="server" ControlToValidate ="customerPhone" 
                ValidationExpression ="^([\(]{1}[0-9]{3}[\)]{1}[\.| |\-]{0,1}|^[0-9]{3}[\.|\-| ]?)?[0-9]{3}(\.|\-| )?[0-9]{4}$" 
                ErrorMessage="Invalid Phone Number"></asp:RegularExpressionValidator>
            <br /><br />
            <label>Email:</label>
            <asp:TextBox runat="server" ID="customerEmail" placeHolder="test@gmail.com"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ID="validatorEmail" ControlToValidate="customerEmail"
                ErrorMessage="Please enter email."></asp:RequiredFieldValidator>
            <%-- Got reference from Christine's code --%>
            <asp:RegularExpressionValidator ID="emailRegexValid" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                ControlToValidate="customerEmail" ErrorMessage="Invalid Email ID"></asp:RegularExpressionValidator>
            <br /><br />
            </fieldset>
            <fieldset><legend>Delivery Details</legend>
            <%-- Delivery Information --%>
            <label>Address:</label>
            <asp:TextBox runat="server" ID="deliveryAddress" ></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ID="validatorAddress" ControlToValidate="deliveryAddress"
                ErrorMessage="Please provide address."></asp:RequiredFieldValidator>
            <br /><br />
            <label>City:</label>
            <asp:TextBox runat="server" ID="deliveryCity" ></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ID="ValidatorCity" ControlToValidate="deliveryCity"
                ErrorMessage="Please provide city."></asp:RequiredFieldValidator>
            <br /><br />
            <label>Province:</label>
            <asp:TextBox runat="server" ID="deliveyProvince" ></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ID="validatorProvince" ControlToValidate="deliveyProvince"
                ErrorMessage="Please provide province."></asp:RequiredFieldValidator>
            <br /><br />
            <label>Country:</label>
            <asp:TextBox runat="server" ID="deliveryCountry" ></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ID="validatorCountry" ControlToValidate="deliveryCountry"
                ErrorMessage="Please provide country name."></asp:RequiredFieldValidator>
            <br /><br />
            <label>Postal code:</label>
            <asp:TextBox runat="server" ID="deliveryPostal" ></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ID="validatorPostal" ControlToValidate="deliveryPostal"
                ErrorMessage="Please provide postal code."></asp:RequiredFieldValidator>
            <br /><br />
            <label>Select Delivery Date</label><asp:Calendar ID="deliveryCal" runat="server" OnSelectionChanged="DateChange">
            </asp:Calendar>
            <br /><br />
            <label>Delivery Date</label>
            <asp:TextBox ID="deliveryDate" runat="server"></asp:TextBox>
            <br/><br />
            </fieldset>

            <fieldset><legend>Item Details</legend>
            <%-- Item information --%>
            <label>Select Item</label>
            <asp:DropDownList runat="server" ID="item">
                <asp:ListItem Value="neckpiece" Text="Neck piece"></asp:ListItem>
                <asp:ListItem Value="earings" Text="Earings"></asp:ListItem>
                <asp:ListItem Value="bracelet" Text="Bracelet"></asp:ListItem>
            </asp:DropDownList>
            <br /><br />
            <label>Size</label><br />
            <%-- <asp:RadioButton runat="server" Text ="Small" GroupName ="size" />
            <asp:RadioButton runat="server" Text ="Medium" GroupName ="size" />
            <asp:RadioButton runat="server" Text ="Large" GroupName ="size" />
            <br /><br />--%>
            <asp:RadioButtonList ID="size" runat="server" RepeatLayout="Flow">
                <asp:ListItem Value="Small">Small</asp:ListItem>
                <asp:ListItem Value="Medium">Medium</asp:ListItem>
                <asp:ListItem Value="Large">Large</asp:ListItem>
            </asp:RadioButtonList>
            <br /><br />
            <label>Colour</label>
            <div id ="itemcolors" runat="server">
            <asp:CheckBox runat="server" ID="gold" Text="Gold" />
            <asp:CheckBox runat="server" ID="silver" Text="Silver" />
            <asp:CheckBox runat="server" ID="rosegold" Text="Rosegold" />
            </div>
            <br /><br />
            <label>Quantity</label>
            <asp:TextBox runat="server" ID="itemQty" placeHolder ="1-5"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ID="validatorQty" ControlToValidate="itemQty"
                ErrorMessage="Please enter quantity."></asp:RequiredFieldValidator>
            <%--  Got reference from Christine's code --%> 
            <asp:RangeValidator ID="qtyValidate" runat="server" ControlToValidate="itemQty" 
                MinimumValue="1" Type="Integer" MaximumValue="5" ErrorMessage="Please enter quntity between 1-5"></asp:RangeValidator>
            <br /><br />


            <asp:Button runat="server" ID="button" OnClick="Order" Text="Submit" />
            <br /><br />
            <asp:ValidationSummary ID ="validationSummary" runat="server" HeaderText="Please fix the followings...." ShowMessageBox ="false" 
                FontColor ="red" Font-Italic="true"/>
            </fieldset>
            <fieldset>
            <div runat="server" id="OrderDet">

            </div>
            </fieldset>
        </div>
    </form>
</body>
</html>
