﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment_1a
{
    public class Order
    {
        public Customer customer;
        public Item item;
        public Delivery delivery;

        public Order(Customer c, Item i, Delivery d)
        {
            customer = c;
            item = i;
            delivery = d;
        }
        //Reference from Cristine's code
        public string printInvoice()
        {
            string invoice = "Order details:<br/>";
            invoice += "Your Total is: " + CalculateTotal().ToString() + "<br/>";
            invoice += "Name : "+ customer.customerFirstName + " ";
            invoice += customer.customerLastName + "<br/>";
            invoice += "Email: " + customer.customerEmail + "<br/>";
            invoice += "Phone: " + customer.customerPhone + "<br/>";
            invoice += "Item: " + item.itemname + "<br/>";
            invoice += "Size: " + item.size + "<br/>";
            invoice += "Colour of item: " + String.Join(", ", item.color.ToArray()) + "<br/>";
            invoice += "Delivery to: " + delivery.DAddress + "<br/> " + delivery.DCity + ", ";
            invoice += delivery.DProvince +", "+ delivery.DCountry + "<br/>" + delivery.DPostal;
            invoice += "<br/>On " + delivery.DDate.ToString("dd/MM/yyyy");

            return invoice;    
        }

        public double CalculateTotal()
        {
            double total = 0;
            if(item.size == "Small")
            {
                total = 10.50;
            }else if(item.size == "Medium")
            {
                total = 15;
            }else if(item.size == "Large")
            {
                total = 20;
            }

            total = total * item.qty;
            return total;
        }

    }
}