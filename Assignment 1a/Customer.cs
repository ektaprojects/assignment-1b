﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment_1a
{
    public class Customer
    {
        public string customerFirstName;
        public string customerLastName;
        public string customerPhone;
        public string customerEmail;

        public Customer(string fname,string lname,string phone,string email)
        {
            customerFirstName = fname;
            customerLastName = lname;
            customerPhone = phone;
            customerEmail = email;
        }

    }
}