﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment_1a
{
    public class Item
    {
        public string itemname;
        public string size;
        public List<string> color;
        public int qty;

        public Item(string i,string s,List<string> clr, int q)
        {
            itemname = i;
            size = s;
            color = clr;
            qty = q;
        }
    }
}