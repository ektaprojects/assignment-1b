﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment_1a
{
    public class Delivery
    {
        private string address;
        private string city;
        private string province;
        private string country;
        private string postal;
        private DateTime date;


        public string DAddress
        {
            get { return address; }
            set { address = value; }
        }
        public string DCity
        {
            get { return city; }
            set { city = value; }
        }
        public string DProvince
        {
            get { return province; }
            set { province = value; }
        }
        public string DCountry
        {
            get { return country; }
            set { country = value; }
        }
        public string DPostal
        {
            get { return postal; }
            set { postal = value; }
        }
        public DateTime DDate
        {
            get { return date; }
            set { date = value; }
        }

    }
}